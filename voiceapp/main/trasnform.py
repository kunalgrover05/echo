import numpy as np
import wave
 
n = -5 # this is how the pitch should change, positive integers increase the frequency, negative integers decrease it.
chunk = 100
CHANNELS = 1
RATE = 41000
RECORD_SECONDS = 5
swidth = 2
  
def transform(filename, n):
    n = n-5

    w = wave.open(filename)
    o = wave.open("processed/"+filename, "w")
    o.setnchannels(w.getnchannels())   
    o.setparams(w.getparams())   
    while True:
        data = w.readframes(chunk)
        if not data:
            break
        data = np.array(wave.struct.unpack("%dh"%(len(data)/swidth), data))

        # do real fast Fourier transform
        data = np.fft.rfft(data)

        # This does the shifting
        data2 = [0]*len(data)
        if n >= 0:
            data2[n:len(data)] = data[0:(len(data)-n)]
            data2[0:n] = data[(len(data)-n):len(data)]
        else:
            data2[0:(len(data)+n)] = data[-n:len(data)]
            data2[(len(data)+n):len(data)] = data[0:-n]

        data = np.array(data2)
        # Done shifting

        # inverse transform to get back to temporal data
        data = np.fft.irfft(data)

        dataout = np.array(data, dtype='int16')
        chunkout = wave.struct.pack("%dh"%(len(dataout)), *list(dataout)) #convert back to 16-bit data
        o.writeframes(chunkout)
    w.close() 
    o.close()
    print "* done"
    return "processed/"+filename

