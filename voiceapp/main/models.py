from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from trasnform import transform

class FileModel(models.Model):
    file = models.FileField(upload_to='files/', null=False)
    creator = models.ForeignKey(User)
    audio_caption = models.FileField(upload_to='files/', null=True)
    text_caption = models.TextField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    mode = models.IntegerField(default=5)
    processed_file = models.FileField(upload_to='files/', null=True)


@receiver(post_save, sender=FileModel)
def modify_file(sender, instance, **kwargs):
	if not kwargs.get('created'):
		return
	print instance.file.url
	print "Done"
	instance.processed_file = transform(instance.audio_caption.url, instance.mode)
	instance.save()


class Comment(models.Model):
    audio_comment = models.FileField(upload_to='files/', null=True)
    text_comment = models.TextField(null=True)
    creator = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
