from django.contrib.auth.models import User, Group
from rest_framework import viewsets

from serializers import UserSerializer, GroupSerializer
from rest_framework.parsers import FormParser, MultiPartParser

from models import FileModel
from serializers import FileUploadSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class FileUploadViewSet(viewsets.ModelViewSet):
    queryset = FileModel.objects.all()
    serializer_class = FileUploadSerializer
    parser_classes = (MultiPartParser, FormParser,)

    # def perform_create(self, serializer):
    #     serializer.save(creator=self.request.user,
    #                     file=self.request.data.get('file'))
